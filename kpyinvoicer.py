#!/usr/bin/env python3
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Eike Hein <hein@kde.org>

import configparser
import copy
import csv
import decimal
import locale
import sys
from datetime import date
from decimal import Decimal
from jinja2 import Template
from pychromepdf import ChromePDF

class InvoiceError(Exception):
    pass

class Invoice:
    last_invoice = 0

    def __init__(self, row):
        row = copy.deepcopy(row)

        for field in ('date',
                      'recipient',
                      'vat_id',
                      'invoice_no',
                      'purchase_order_no',
                      'comment',
                      'currency',
                      'jurisdiction',
                      'flags'):
            setattr(self, '_' + field, row.pop(0))

        self._items = []

        if len(self._invoice_no):
            try:
                Invoice.last_invoice = int(self._invoice_no)
            except ValueError:
                print("ERROR: Invoice no. in record is not a number or otherwise corrupted.")
                raise InvoiceError()
        else:
            Invoice.last_invoice += 1
            self._invoice_no = Invoice.last_invoice

        while len(row) >= 2:
            title = row.pop(0)
            amount = row.pop(0)

            if not len(title):
                print("ERROR: Line item in record has empty title.")
                raise InvoiceError()

            if not len(amount):
                print("ERROR: Line item in record has empty amount.")
                raise InvoiceError()

            try:
                amount = Decimal(amount)
            except decimal.InvalidOperation:
                print("ERROR: Amount for line item can not be parsed.")
                raise InvoiceError()

            self._items.append((title, amount))

        if not len(self._items):
            print("ERROR: Record has no valid line items. Perhaps you forgot the amount?")
            raise InvoiceError()

        if len(self._recipient) and self._recipient[0] == '%' and len(contacts.sections()):
            recipient_name = self._recipient[1:]
            self._recipient = contacts[recipient_name]

        if self.jurisdiction() in ('de', 'eu', 'us', 'non_eu'):
            if self.jurisdiction() == 'de' and not 'm' in self._flags:
                current_total = self.total()
                self._items.append(('VAT {}%'.format(config['general']['vat']),
                    (Decimal(config['general']['vat']) * current_total) / Decimal(100)))
            elif self.jurisdiction() == 'eu' and not len(self.vat_id()):
                print("ERROR: Record has a jurisdiction of 'eu' but no VAT id! Problematic row follows:")
                raise InvoiceError()
        else:
            print("ERROR: Record with unknown jurisdiction of '{}'.".format(self.jurisdiction()))
            raise InvoiceError()

    def date(self):
        return date.fromisoformat(self._date) if len(self._date) else date.today()
        return invoice_date.strftime(config['general']['date_format'])

    def recipient(self):
        if isinstance(self._recipient, str):
            return self._recipient
        else:
            address = []
            for line in range(10):
                option_name = 'address' + str(line)
                if option_name in self._recipient:
                    address.append(self._recipient[option_name])
            return '\\n'.join(address)

    def vat_id(self):
        if isinstance(self._recipient, str):
            return self._vat_id
        else:
            try:
                return self._recipient['vat_id']
            except KeyError:
                return ''

    def invoice_no(self):
        return int(self._invoice_no)

    def purchase_order_no(self):
        return self._purchase_order_no

    def comment(self):
        return self._comment
    
    def currency(self):
        return self._currency.upper()

    def jurisdiction(self):
        if len(self._jurisdiction):
            return self._jurisdiction
        elif not isinstance(self._recipient, str):
            return self._recipient['jurisdiction']
        else:
            return ''

    def flags(self):
        return self._flags

    def is_corrected(self):
        if 'c' in self._flags:
            return True
        else:
            return False

    def is_reversal(self):
        if 'r' in self._flags:
            return True
        else:
            return False

    def is_draft(self):
        if 'd' in self._flags:
            return True
        else:
            return False

    def items(self):
        return self._items

    def total(self):
        return Decimal(sum(item[1] for item in self._items))

    def filename(self):
        recipient = self._recipient.name if not isinstance(self._recipient, str) else self._recipient.split('\\n')[0].lower()
        summary = 'membershipfee' if 'm' in self._flags else self.items()[0][0].split()[0].lower()
        return '{}-{:02d}-{}-{}.pdf'.format(self.date().strftime("%Y"), self.invoice_no(),
            summary, recipient)

if __name__ == "__main__":
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

    invoices = []

    try:
        config = configparser.ConfigParser(interpolation=None)
        with open('config.ini', 'r') as config_file:
            config.read_file(config_file)
    except OSError:
        sys.exit("ERROR: Unable to open config.ini. Place in current directory.")

    try:
        contacts = configparser.ConfigParser()
        with open('contacts.ini', 'r') as contacts_file:
            contacts.read_file(contacts_file)
    except OSError:
        print("WARNING: Unable to open contacts file! Referencing contacts as recipient will NOT work.")
    except configparser.ERROR:
        print("WARNING: Error parsing contacts file. Referencing contacts as recipient will NOT work.")

    try:
        with open(config['general']['input_filename'], 'r') as csvfile:
            record_reader = csv.reader(csvfile)
            for row in record_reader:
                if not len(row) or (len(row[0]) and row[0][0] == '#'):
                    continue

                try:
                    invoices.append(Invoice(row))
                except InvoiceError:
                    sys.exit("Aborting due to fatal record error. Problematic record:\n{}".format(row))
    except OSError:
        print("ERROR: Unable to open input file specified in config file, aborting. In detail:")
        raise

    for invoice in invoices:
        try:
            with open(config['general']['template_filename'], 'r') as template_file:
                template = Template(template_file.read())

                try:
                    with open(invoice.filename(),'w') as output_file:
                        ChromePDF._chrome_options.append('--print-to-pdf-no-header')
                        cpdf = ChromePDF(config['general']['chrome_path'])
                        cpdf.html_to_pdf(template.render(invoice=invoice), output_file)
                except OSError:
                    sys.exit("ERROR: Unable to open '{}' for writing.".format(invoice.filename()))
        except OSError:
            print("ERROR: Unable to open template file specified in config file, aborting. In detail:")
            raise

    print("Invoices done!")

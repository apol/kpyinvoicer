SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
SPDX-FileCopyrightText: 2020 Eike Hein <hein@kde.org>

DISCLAIMER: Please don't take this as legal or tax advise, this is just an
invoice generation tool created to make your life easier and it doesn't expect
to be anything beyond this. Enjoy!

-----

KPyInvoicer generates invoice PDFs for KDE e.V., e.g. for membership fees and
event sponsorships.

It can handle the intricacies of sending invoices to German, EU and non-EU
recipients, including automatic addition of VAT line items and VAT reverse-
charge notices when indicated and setting the correct paper size.

It can do automatic date handling and invoice numbering.

It can handle purchase numbers, invoice corrections and add DRAFT warnings.

Invoices to generate are specified via a csv-formatted input file explained
below. This way, it is easy to generate invoices individually or in bulk,
and consult past records for reference.

Recipient information can optionally be pulled from a contacts database,
avoiding a large amount of repetitive data entry.

The appearance of the generated invoices is supplied by a HTML+CSS template.

-----

Dependencies:
- Python 3.7+
- jinja2
- pychromepdf
- Google Chrome
- (Recommended) The Noto Sans font as system sans-serif, for consistent appearance

-----

config.ini:

[general]
input_filename = <string> # name of input file
template_filename = <string> # name of template file
vat = <int> # percent of VAT to apply as additional line item, if needed
chrome_path = <string> # path to Google Chrome executable

-----

Usage notes:

- Adding multiple line items: Append additional title+amount column pairs (see below).
- If you don't want to generate an invoice for a record, just comment out line with '#'.

-----

Example records:

- A non-tax-deductible invoice to a German recipient (automatically adding a VAT line item):
  2020-01-01,%foo,,15,,,eur,,,Some line item,500

- The same, but with today's date and two line items:
  ,%foo,,15,,,eur,,,Some line item,500,Another line item,200

- The same, but with a manually specified address for an EU recipient (adding reverse-charge
  notice, not having a VAT line item):
  ,"Some Inc.\nSome Street\nSome Country,SC12345678,15,,,eur,eu,,Some line item,500,Another line item,200

- A US recipient, paper size will be US Letter, plus automatic invoice numbering:
  2020-01-01,%foo,,,,,usd,,,Some line item,500

- The same, but with a Purchase Order No. added and a "Corrected" prefix for the Invoice No.:
  2020-01-01,%foo,,123456,,,usd,,c,Some line item,500

- A membership fee to a German recipient - no VAT line item will be added:
  ,"Some Inc.\nSome Street\nSome Country,,15,,,eur,de,m,Patronage,10000

- The same, but with a DRAFT warning in the page background:
  ,"Some Inc.\nSome Street\nSome Country,,15,,,eur,de,m,Patronage,10000

-----

Columns:

date:              empty for today or ISO 8061

recipient:         %contact_name (cf. contacts.ini) or address in double-quotes w/ newlines (written as \n)

vat_id:            empty to retrieve from contact (cf. contacts.ini) or specify

invoice_no:        empty for last+1 (no previous records results in invoice no. of 1) or specify

purchase_order_no: specify if available

comment:           a comment added below the invoice_no and purchase_order_no

currency:          eur, usd, ...

jurisdiction:      empty to retrieve from contact (cf. contacts.ini) or one of de, eu, us, non_eu
                   de:         A4 paper size; adds VAT line item
                   eu:         A4 paper size; no VAT line item, adds notice about reverse-charge (requires
                               vat_id from contacts.ini or record, error otherwise
                   us, non_eu: US Letter paper size, no VAT, no mention of VAT reverse-charge

flags:             string of known characters, each modifies the generated invoice
                   unknown flags are ignored silently
                   known flags:
                   c: corrected, adds "Corrected " in front of "Invoice No. ..." field
                   d: draft, adds a DRAFT notice to the generated invoice
                   m: membership fee, means no VAT even for 'de' jurisdiction records
                   r: this is a reversal of an already-paid invoice. replaces the payment instructions
                      with an apology.

title:             invoice line item title in double-quotes

amount:            line item monetary value
